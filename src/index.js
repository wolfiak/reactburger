import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { createStore,applyMiddleware,compose, combineReducers } from 'redux';  
import { Provider} from 'react-redux';
import thunk from 'redux-thunk';
import Reducer from './store/reducers/burgerBuilder';
import OrderReducer from './store/reducers/order';
import AuthReducer from './store/reducers/auth';
const rootReducer=combineReducers({
    burgerBuilder: Reducer,
    order: OrderReducer,
    auth: AuthReducer
});

const composeEnhancers = process.env.NODE_ENV === 'development' ?  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;
const store=createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));



ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
