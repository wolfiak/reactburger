export const checkValidity=(value,rules)=>{

    let values=true;
    if(!rules){
        return true;
    }
    if(rules.required){
        values= value.trim().length > 0 && values;
    }
    if(rules.minLenth){
        values= value.trim().length >=rules.minLenth && values;
    }
    if(rules.maxLength){
        values= value.trim().length <rules.maxLength && values;
    }

    return values;
}