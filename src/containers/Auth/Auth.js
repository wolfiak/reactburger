import React, {Component} from 'react';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import styles from './Auth.css';
import Spinner from '../../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router-dom'
import { checkValidity } from '../../shared/validation'; 

import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';
class Auth extends Component {
    state={
        controls: {
            email: {
                elementType: 'input',
                elementConfig:{
                    type: 'email',
                    placeholder: 'Podaj Email'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig:{
                    type: 'password',
                    placeholder: 'Haslo'
                },
                value: '',
                validation: {
                    required: true,
                    minLenght: 6
                },
                valid: false,
                touched: false
            }
        },
        isSignUp: true
    }

    componentDidMount(){
        if(!this.props.buldingBurger && this.props.authRedirect !== '/'){
            this.props.onSetAuthRedirect();
        }
    }

    inputChangeHandler = (event, controlName)=>{
        const updated= {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: checkValidity(event.target.value,this.state.controls[controlName].validation),
                touched: true
            }
        }
        this.setState({
            controls: updated
        })
    }
    onSubmitHandler=(event)=>{
        event.preventDefault();
       
        this.props.onAuth(this.state.controls.email.value,this.state.controls.password.value,this.state.isSignUp)
    }
    switchAuthModeHadnler= (event)=>{
        event.preventDefault();
        this.setState(prevState=>{
            return {
                isSignUp: !prevState.isSignUp
            }
        });
    }
    render(){
        const tablica=[];
        for (let key in this.state.controls){
            tablica.push({
                id: key,
                config: this.state.controls[key]

            })
        }
      
        const from = tablica.map(elm =>{
            return(
                <Input  key={elm.id} 
                     elementType={elm.config.elementType}
                     elementConfig={elm.config.elementConfig} value={elm.config.value}
                     changed={(event)=> this.inputChangeHandler(event,elm.id)}
                     invalid={!elm.config.valid} 
                     shouldValidate={elm.config.validation}
                     touched={elm.config.touched}/>
            );
        })
        let loading= (
            <form onSubmit={this.onSubmitHandler}>
            {from}
            <Button btnType="Success" >
                SUBMIT
            </Button>
            <Button btnType="Danger"
                clicked={this.switchAuthModeHadnler}> SWITCH TO  {this.state.isSignUp ? 'SIGN IN' : 'SIGN UP'}
            
            </Button>
        </form>
        );
        if(this.props.loading){
            loading= <Spinner />
        }
        let errorMessage=null;
        if(this.props.error){
            errorMessage= <p>{this.props.error.message}</p>
        }
        let isAuth=null;
        if(this.props.isAuth){
            isAuth= <Redirect to={this.props.authRedirect} />
        }
        return(
            <div className={styles.Auth}>
               {errorMessage}
               {loading}
                {isAuth}
            </div>
        );
    }
}
const mapStateToProps= state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuth:state.auth.token !== null,
        buldingBurger: state.burgerBuilder.bulding,
        authRedirect: state.auth.authRedirect 
    }
}
const mapDispatchToProps= dispatch =>{
    return {
        onAuth: (email,password,isSignUp)=> dispatch(actions.authAsync(email,password,isSignUp)),
        onSetAuthRedirect: ()=> dispatch(actions.setAuthRedirect('/'))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Auth);