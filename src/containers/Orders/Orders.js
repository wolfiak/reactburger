import React, {Component} from 'react';
import Order from '../../components/UserOrder/Order';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
class Orders extends Component{
 
    componentDidMount(){
       
        this.props.onFetchOrders(this.props.token,this.props.userId);
    }

    render(){
        let ordero= <Spinner />
        if(!this.props.loading){
            ordero=this.props.orders.map(elm=>{
           
                const skladnik=[];
                for(let s in elm.skladniki){
                    if(elm.skladniki[s] > 0)
                    skladnik.push(s+`(${elm.skladniki[s]})`);
                }
                const cena=+elm.price;
                return <Order key={elm.id} skladniki={skladnik.join(', ') } price={cena.toFixed(2)} />
            });
        }
    
        return(
            <div>
               {ordero}
              
            </div>
            
        );
    }
}
const mapStateToProps= state =>{
    return {
        orders: state.order.orders,
        loading: state.order.loading.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}
const mapDispatchToProps = dispatch =>{
    return {
        onFetchOrders: (token,id)=> dispatch(actions.fetchOrdersAsync(token,id))
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Orders,axios));