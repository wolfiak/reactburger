import React, {Component} from 'react';
import Aux from '../../hoc/Auxo';

import axios from '../../axios-orders';

import Spinner from '../../components/UI/Spinner/Spinner';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import OrderSummery from '../../components/Burger/OrderSummery/OrderSummery';
import Modal from '../../components/UI/Modal/Modal';
import WithErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';




export class BurgerBuilder extends Component{
    state={
        ordered: false
    }
    componentDidMount(){
      this.props.onInitIngredients();
    }
    updatePurchaseState(inggredients){
        const sum=Object.keys(inggredients).map(elem=>{
            return inggredients[elem]
        }).reduce((pre,curr)=>{
            return pre + curr;
        },0);
           return sum >0
    
    }

    
    orderNowMode=()=>{
        if(this.props.isAuth){
            this.setState({
                ordered: true
            })
        }else{
            this.props.onSetRedirect('/checkout')
            this.props.history.push('/auth');
        }
       
    }
    modalClosed=()=>{
        this.setState({ordered: false})
    }

    purchaseContinueHanler=()=>{
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
     
    }

    render(){
        const disabledInfo={
            ...this.props.ingredients
        };
        for(let key in disabledInfo){
            disabledInfo[key]= disabledInfo[key] <=0;
        }
        let summery= <OrderSummery ingredients={this.props.ingredients} purchase={this.purchaseContinueHanler} 
            cancel={this.modalClosed}
            price={this.props.totalPrice}/>
    
        let burgero= this.props.error ? <p>Nie mozna wyswietlic skladinow</p> : <Spinner />;
        if(this.props.ingredients){
            burgero= (
                <Aux>
                    <Burger ingredients={this.props.ingredients}></Burger>
                    <BuildControls 
                        disabledInfo={disabledInfo}
                        ingredientAdd={this.props.onAddIngredients} 
                        ingredientDelete={this.props.onDeleteIngredients}
                        price={this.props.totalPrice}
                        purchaseble={this.updatePurchaseState(this.props.ingredients)}
                        ordering={this.orderNowMode}
                        isAuth={this.props.isAuth}></BuildControls>
                </Aux>
            );
        }
        return(
            <Aux>
                <Modal show={this.state.ordered} modalClosed={this.modalClosed}>
                    {summery}
                </Modal>
                {burgero}
             
            </Aux>
        );
    }

}
const mapStateToProps= state =>{
   return{
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.error,
        isAuth: state.auth.token !==null
   }
}
const mapDispatchToProps= disptach=>{
    return{
        onAddIngredients: (iName)=> disptach(actions.addIngredient(iName)),
        onDeleteIngredients: (iName)=> disptach(actions.removeIngredient(iName)),
        onInitIngredients: () => disptach(actions.initIngredients()),
        onInitPurchase: () => disptach(actions.purchaseInit()),
        onSetRedirect: (path) => disptach(actions.setAuthRedirect(path))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(WithErrorHandler(BurgerBuilder,axios));