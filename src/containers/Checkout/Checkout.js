import React,{ Component } from 'react';
import CheckoutSummery from '../../components/Order/CheckoutSummery/CheckoutSummery';
import {Route, Redirect} from 'react-router-dom';
import ContactData from '../ContactData/ContactData';
import { connect } from 'react-redux';
class Checkout extends Component{
 
   
    handleCancle=()=>{
        this.props.history.goBack();
    }

    handleContinue=()=>{
        this.props.history.replace('/checkout/contact-data');
    }
    render(){
        let summery= <Redirect to="/"/>
        
        if(this.props.ingredients){
            const purchasedRedirect = this.props.pruchased ? <Redirect to="/"/> : null
            summery=(
            <div>
                <CheckoutSummery handleContinue={this.handleContinue} handleCancle={this.handleCancle} ingredients={this.props.ingredients} />
                <Route path={this.props.match.path + '/contact-data'} 
                    component={ContactData}/>
                {purchasedRedirect}
            </div>
            );
        }
        return summery;
    }
}

const mapStateToProps = state=>{
    return{
        ingredients: state.burgerBuilder.ingredients,
        pruchased : state.order.purchased
    }
}


export default connect(mapStateToProps)(Checkout);