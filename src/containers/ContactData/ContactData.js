import React, {Component} from 'react';
import Button from '../../components/UI/Button/Button';
import styles from './ContactData.css';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import Input  from '../../components/UI/Input/Input';
import {connect} from 'react-redux';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index'; 
import { checkValidity } from '../../shared/validation';

class ContactData extends Component{
    state={
        orderForm:{
            name: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Podaj imie'
                },
                value: '',
                validation: {
                    required: true,
                    minLenth: 5
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Podaj Ulice'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Kod Pocztowy'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 5
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Kraj'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig:{
                    type: 'email',
                    placeholder: 'Podaj Email'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig:{
                  options:[
                      {value: 'fastest', display: 'Fastest'},
                      {value: 'cheapests', display: 'Cheapests'}
                  ]
                },
                value: 'fastest',
                validation: {},
                valid: true
            }
        },
        formIsValid: false
    }

    orderHandler=(event)=>{
        event.preventDefault();
        const formData={}
        for (let elm in this.state.orderForm){
            formData[elm]=this.state.orderForm[elm].value;
        }
        const order={
            skladniki: this.props.ingredients,
            price: this.props.totalPrice,
            orderData: formData,
            userId: this.props.userId
            
        }
        this.props.onOrderBurger(order, this.props.token);
       
    }
    inputChangeHandler=(event, id)=>{
        const form={
            ...this.state.orderForm
        };
        const updatedForm={
            ...form[id]
        };
        updatedForm.value=event.target.value;
        updatedForm.valid=checkValidity(updatedForm.value,updatedForm.validation);
        updatedForm.touched=true;
        form[id]=updatedForm;
        let formIsValid=true;
   
        for(let input in form){
       
            formIsValid=form[input].valid && formIsValid;
            
        }
        this.setState({
            orderForm: form,
            formIsValid: formIsValid
        })
    }
    render(){
        const tablica=[];
        for (let key in this.state.orderForm){
            tablica.push({
                id: key,
                config: this.state.orderForm[key]

            })
        }
        let form =(
            <form onSubmit={this.orderHandler}>
                {tablica.map(elm=>{
                    
                    return  <Input key={elm.id} elementType={elm.config.elementType}
                     elementConfig={elm.config.elementConfig} value={elm.config.value}
                     changed={(event)=> this.inputChangeHandler(event,elm.id)}
                     invalid={!elm.config.valid} 
                     shouldValidate={elm.config.validation}
                     touched={elm.config.touched}/>
                })}
               
                
                <Button  btnType="Success" disabled={!this.state.formIsValid}>Dodaj</Button>
            </form>   
        );
        if(this.props.loading){
            form=<Spinner/>
        }


        
        return(
            <div className={styles.ContactData}>
                <h4>Podaj dane kontaktowe</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps= state=>{
    return{
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}
const mapDispatchToProps= dispatch => {
    return {
        onOrderBurger: (orderData,token)=> dispatch(actions.purchaseBurgerSuccessAsync(orderData,token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios));