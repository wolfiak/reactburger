import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Logout from './containers/Auth/Logout/Logout';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import asyncComponent from './hoc/asyncComponent/asyncComponent';

const asyncCheckout = asyncComponent(()=>{
  return import('./containers/Checkout/Checkout');
});
const asyncOrders = asyncComponent(()=>{
  return import('./containers/Orders/Orders');
})
const asyncAuth = asyncComponent(()=>{
  return import('./containers/Auth/Auth');
})

class App extends Component {

  componentDidMount() {
    this.props.onTrySignIn();
  }

  render() {

    let routes=(
      <Switch>
        <Route path="/auth" component={asyncAuth} />
        <Route path="/" component={BurgerBuilder}/>
        <Redirect to="/" />
      </Switch>
    );
    if(this.props.isAuth){
      routes=(
        <Switch>
          <Route path="/checkout" component={asyncCheckout}/>
          <Route path="/orders" component={asyncOrders} />
          <Route path="/logout" component={Logout} />
          <Route path="/auth" component={asyncAuth} />
          <Route path="/" component={BurgerBuilder}/>
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <BrowserRouter>
        <Layout>
           {routes}
         </Layout>
      </BrowserRouter>
    );
  }
}
const mapStateToprops= state =>{
  return {
    isAuth: state.auth.token !== null
  }
}
const mapDispatchtoProps= dispatch =>{
  return {
    onTrySignIn: ()=> dispatch(actions.authCheckState())
  }
}
export default  connect(mapStateToprops, mapDispatchtoProps)(App);
