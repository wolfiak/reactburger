import React, {Component} from 'react';
import {connect} from 'react-redux';
import { withRouter} from 'react-router-dom';
import Aux from '../../hoc/Auxo';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';
import styles from './Layout.css';
import Hamburger from '../UI/Hamburger/Hamburger';

import PropTypes from 'prop-types';

class Layout extends Component{
    state={
        showSideDrawer: false
    }
    sideDrawerClosedHandler=()=>{
        this.setState({showSideDrawer: false})
    }
    sideDrawerOpenHandler=()=>{
        this.setState({showSideDrawer: true})
    }

    render(){
        return(
            <Aux>
            <Toolbar isAuth={this.props.isAuth}><Hamburger clicked={this.sideDrawerOpenHandler} rev/></Toolbar>
            <SideDrawer  isAuth={this.props.isAuth} open={this.state.showSideDrawer} closed={this.sideDrawerClosedHandler}>
                <Hamburger clicked={this.sideDrawerClosedHandler}/>
            </SideDrawer>
            <main className={styles.Content}>
                {this.props.children}
            </main>
            </Aux>
        );

    }

}
Layout.propTypes={
    showSideDrawer: PropTypes.bool
}

const mapStateToProps= state => {
    return {
        isAuth: state.auth.token !== null
    }
}

export default withRouter(connect(mapStateToProps)(Layout));