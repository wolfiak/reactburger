import React from 'react';
import styles from './Burger.css';

import BurgerIngrediant from './BurgerIngridient/BurgerIngrediant';

const burger=(props) => {
    let transformIngredients=Object.keys(props.ingredients)
        .map(igKey =>{
            // console.log(igKey);
            // console.log(props.ingredients);
            return [...Array(props.ingredients[igKey])].map((_,i)=>{
                return <BurgerIngrediant key={igKey+i} type={igKey}></BurgerIngrediant>;
            });
        }).reduce((pre,cur)=> pre.concat(cur) ,[]);
        //console.log(transformIngredients);
        if(!transformIngredients.length){
            transformIngredients=<p>Dodaj jakieś składniki!</p>
        }
    return(
        <div className={styles.Burger}>
            <BurgerIngrediant type="bread-top"></BurgerIngrediant>
            {transformIngredients}
            <BurgerIngrediant type="bread-bottom"></BurgerIngrediant>
        </div>
    );
}

export default burger;