import React from 'react';

import styles from './BuildControls.css';
import BuildControl from  './BuildControl/BuildControl';
const controls=[
    {label: 'Salad', type:'salad'},
    {label: 'Bacon', type:'bacon'},
    {label: 'Cheese', type:'cheese'},
    {label: 'Meat', type:'meat'}
]


const buildControls=(props)=>{

    let butts=( 
        <button 
        disabled={!props.purchaseble}
        className={styles.OrderButton}
        onClick={props.ordering}>SIGN IN</button>
    );
    if(props.isAuth){
         butts=( 
            <button 
            disabled={!props.purchaseble}
            className={styles.OrderButton}
            onClick={props.ordering}>ORDER NOW</button>
        );
    }
    return(
        <div className={styles.BuildControls}>
            <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
            {controls.map(elem =>{
                return <BuildControl 
                disabledInfo={props.disabledInfo[elem.type]} 
                add={()=> props.ingredientAdd(elem.type)} 
                delete={()=> props.ingredientDelete(elem.type)} key={elem.label} 
                label={elem.label}>
                </BuildControl>
            })}
           {butts}
        </div>
    );
    };

export default buildControls;