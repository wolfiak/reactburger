import React, {Component} from 'react'

import Aux from '../../../hoc/Auxo';
import Button from '../../UI/Button/Button';


class OrderSummery extends Component{
    
    

    render(){
        let skladniki=null;
       if(this.props.ingredients) {
         skladniki=Object.keys(this.props.ingredients).map(elm=>{
           
            if(this.props.ingredients[elm] > 0){
                return <li key={elm}>x<strong>{this.props.ingredients[elm]}</strong> <span style={{textTransform: 'capitalize'}}>{elm}</span> </li>
            }else{
                return null;
            }
           
        });
       }
   
        return(
            <Aux>
                <h3>Twoje zamowienie</h3>
                <p>Twoje skladniki:</p>
                <ul>
                    {skladniki}
                </ul>
                <p><strong>Suma: {this.props.price.toFixed(2)}</strong></p>
                <Button clicked={this.props.cancel} btnType="Danger">CANCEL</Button>
                <Button clicked={this.props.purchase} btnType="Success">CONTINUE</Button>
            </Aux>
    
        );
    }
}



export default OrderSummery;