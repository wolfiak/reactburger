import React from 'react';
import styles from './Order.css';
const order =(props)=>(
    <div className={styles.Order}>
        <p>Skladniki: {props.skladniki} </p>
        <p>Price: <strong>{props.price}</strong></p>
    </div>
);

export default order;