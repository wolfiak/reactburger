import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faBars from '@fortawesome/fontawesome-free-solid/faBars';

import styles from './Hamburger.css';

const hamburger=(props)=>{
    let styl=[styles.Hamburger];
    if(props.rev){
        styl.push(styles.White);
    }

    return(
        <FontAwesomeIcon 
            onClick={props.clicked}
            className={styl.join(' ')} 
            icon={faBars}/>
    );
}
export default hamburger;