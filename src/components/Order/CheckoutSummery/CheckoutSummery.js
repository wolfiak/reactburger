import React from 'react';
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

import styles from './CheckoutSummery.css';

const checkoutSummery=(props) =>{




    return (
        <div className={styles.CheckoutSummery}>
            <h1>Oto towj burger</h1>
            <div style={{width: '100%',margin: 'auto'}}>   
                <Burger ingredients={props.ingredients} />
            </div>
            <Button clicked={props.handleCancle} btnType="Danger">CANCLE</Button>
            <Button clicked={props.handleContinue} btnType="Success">CONTINUE</Button>
        </div>
    );
}

export default checkoutSummery;