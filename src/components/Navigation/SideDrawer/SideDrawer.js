import React from 'react';

import styles from './SideDrawer.css';

import Logo from '../../Logo/Logo';
import NavigationsItems from '../NavigationsItems/NavigationsItems';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/Auxo';

const sideDrawer=(props)=>{
    let attachedStyles=[styles.SideDrawer, styles.Close];
    if(props.open){
        attachedStyles=[styles.SideDrawer,styles.Open]
    }

    return(
        <Aux>
            <Backdrop show={props.open} clicked={props.closed}/>
            <div className={attachedStyles.join(' ')} onClick={props.closed}>
                <div className={styles.Logo}>
                    <Logo  />
                    {props.children}
                </div>
            
                <nav>
                    <NavigationsItems isAuth={props.isAuth}/>
                </nav>
            </div>
        </Aux>
    );
}

export default sideDrawer;