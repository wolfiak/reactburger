import React from 'react';

import NavigationsItem from './NavigationsItem/NavigationsItem';

import styles from './NavigationItems.css'
const navigationsItems=(props)=>{

    let auth=<NavigationsItem link="/auth" >Sign Up</NavigationsItem>
    if(props.isAuth){
        auth=<NavigationsItem link="/logout" >Logout</NavigationsItem>
    }
    return(
            <ul className={styles.NavigationItems}>
                <NavigationsItem exact link="/">Burger Builder</NavigationsItem>
                {props.isAuth ? <NavigationsItem link="/orders" >Zamowienia</NavigationsItem>: null}
                {auth}
            </ul>
        );
};

export default navigationsItems;