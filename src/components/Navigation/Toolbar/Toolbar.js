import React from 'react';

import NavigationsItems from '../NavigationsItems/NavigationsItems';
import Logo from '../../Logo/Logo';

import styles from './Toolbar.css';

const toolbar=(props)=>(
    <header className={styles.Toolbar}>
        {props.children}
        <Logo height="80%" />
        <nav className={styles.DesktopOnly}>
            <NavigationsItems isAuth={props.isAuth}/>
        </nav>
    </header>
)

export default toolbar;