import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart= () =>{
    return{
        type: actionTypes.AUTH_START
    };
};

export const authSuccess= (data)=>{
    return{
        type: actionTypes.AUTH_SUCCESS,
        authData: data
    };
};
export const authFail= (error)=>{
    return{
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};
export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('experationDate');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.AUTH_LOGOUT,
    }
}

export const checkAuthTimeout = (time) =>{
    return dispatch => {
        setTimeout(()=>{
            dispatch(logout());
        },(time*1000))
    }
}


export const authAsync= (email,password, isSignUp)=>{
    return dispatch =>{
        dispatch(authStart());
        const data={
            email: email,
            password: password,
            returnSecureToken: true
        }
        let url='https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAklZnflMl7Qcx1SHKCqmBnnDC_sxuAack';
        if(!isSignUp){
            url='https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAklZnflMl7Qcx1SHKCqmBnnDC_sxuAack';
        }
        axios.post(url,data)
            .then(res=>{
                const experationDate=new Date(new Date().getTime()+ res.data.expiresIn * 1000);
                localStorage.setItem('token',res.data.idToken);
                localStorage.setItem('experationDate',experationDate);
                localStorage.setItem('userId',res.data.localId)
                dispatch(authSuccess(res.data));
                dispatch(checkAuthTimeout(res.data.expiresIn));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const setAuthRedirect= (path) =>{
    return{
        type: actionTypes.AUTH_SET_REDIRECT,
        path: path
    }
}

export const authCheckState = () =>{
    return dispatch => {
        const token =localStorage.getItem('token');
        if(!token){
            dispatch(logout())
        }else{
            const expTime=new Date(localStorage.getItem('experationDate'));
            if(expTime <= new Date()){
                dispatch(logout())
            }else{
                const userId=localStorage.getItem('userId');
                dispatch(authSuccess({idToken: token, localId: userId}));
                dispatch(checkAuthTimeout((expTime.getTime() - new Date().getTime())/1000));
            }
           
        }

    }
}