import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';


export const addIngredient=(ingredientName)=>{

    return{
        type: actionTypes.ADD_INGREDIENT,
        ingredientName:ingredientName
    };
};

export const removeIngredient=(ingredientName)=>{
    return{
        type:actionTypes.REMOVE_INGREDIENT,
        ingredientName:ingredientName
    };
};
export const setIngredients= (ingredients) => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients: ingredients
    }
}
export const fetchIngredientsFaild= () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_FAILD
    }
}
export const initIngredients= ()=> {
    return disptach => {
        axios.get('https://react-burger-3d3df.firebaseio.com/skladniki.json')
        .then(res=>{
            disptach(setIngredients(res.data));
        })
        .catch(err=>{
            disptach(fetchIngredientsFaild());
        });
    }
}