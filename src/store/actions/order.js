import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const purchaseBurgerSuccess= (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData: orderData
    }
};

export const purchaseBurgerFail= (error) =>{
    return {
        type: actionTypes.PURCHASE_BURGER_FAIL,
        error: error
    }
};
export const purchaseBurgerStart= () =>{
    return{
        type: actionTypes.PURCHASE_BURGER_START

    }
}

export const purchaseBurgerSuccessAsync=  (orderData, token) =>{
    return dispatch =>{
        dispatch(purchaseBurgerStart());
        axios.post('/orders.json?auth='+token, orderData)
        .then(res=> {
            dispatch(purchaseBurgerSuccess(res.data.name, orderData));
        })
        .catch(err=> {
            dispatch(purchaseBurgerFail(err));
        });
    };

}
export const purchaseInit = ()=>{
    return {
        type: actionTypes.PURCHASE_INIT
    }
}

export const fetchOrderSuccess= (orders) =>{
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        orders: orders
    }
};
export const fetchOrdersFail = (error) => {
    return{
        type: actionTypes.FETCH_ORDERS_FAIL,
        error: error
    }
}
export const fetchOrderInit= () =>{
    return {
        type: actionTypes.FETCH_ORDERS_INIT
    }
}
export const fetchOrdersAsync= (token,userId) => {
    return dispatch => {
        dispatch(fetchOrderInit());
        const queryParam=`${token}&orderBy="userId"&equalTo="${userId}"`;
        axios.get('/orders.json?auth='+queryParam)
        .then(res=>{
            let orders=[];
            
            for(let key in res.data){
                orders.push({
                    ...res.data[key],
                    id: key
                });
            }
         dispatch(fetchOrderSuccess(orders));
        }).catch(err=>{
          dispatch(fetchOrdersFail(err));
        });
    }
}