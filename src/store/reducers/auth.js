import * as actionTypes from '../actions/actionTypes';

const initailState={
    token: null,
    userId: null,
    error: null,
    loading: false,
    authRedirect: '/'
};

const reducer = (state=initailState, action) => {

    switch(action.type){

        case(actionTypes.AUTH_START): {
            return{
                ...state,
                error: null,
                loading: true
            };
        }
        case(actionTypes.AUTH_SUCCESS): {
            return{
                ...state,
                userId: action.authData.localId,
                token: action.authData.idToken,
                loading: false,
                error:null
            }
        }
        case(actionTypes.AUTH_FAIL): {
            return {
                ...state,
                error: action.error,
                loading: false
            }
        }
        case(actionTypes.AUTH_LOGOUT):{
            return {
                ...state,
                token: null,
                userId: null
            }
        }
        case(actionTypes.AUTH_SET_REDIRECT): {
            return {
                ...state,
                authRedirect: action.path
            }
        }

        default: {
            return state;
        }
    }
};

export default reducer;