import * as actionT from '../actions/actionTypes';

const initialState={
    ingredients: null,
    error: false,
    totalPrice: 4,
    bulding: false
}

const INGREDIENT_PRICES={
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 2.0
};

const reducer=(state = initialState, action)=>{

    switch(action.type){

        case(actionT.ADD_INGREDIENT): {
           
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName]+1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
                bulding: true
            }
        }
        case(actionT.REMOVE_INGREDIENT): {
           
            return {
                ...state,
                ingredients:{
                     ...state.ingredients,
                    [action.ingredientName]: state.ingredients[action.ingredientName]-1
                 },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingredientName],
                bulding: true
            }
        }
        case(actionT.SET_INGREDIENTS): {
            return {
                ...state,
                ingredients: action.ingredients,
                totalPrice: 4,
                error: false,
                bulding: false
            }
        }
        case(actionT.FETCH_INGREDIENTS_FAILD):{
            return{
                ...state,
                error: true
            }
        }

        default: {
            return state;
        }
    }
}

export default reducer;