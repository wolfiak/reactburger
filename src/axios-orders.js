import axios from 'axios';

const orders=axios.create({
   baseURL:  'https://react-burger-3d3df.firebaseio.com/'
});

export default orders;